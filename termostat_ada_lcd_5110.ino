// python-build-start
// /usr/bin/arduino
// arduino:avr:nano
// /dev/ttyUSB0
// python-build-end

#include <Wire.h>
#include <RTClib.h>
#include <EEPROM.h>
#include <dht.h>
#include <SPI.h>
#include <Adafruit_GFX.h>
#include <Adafruit_PCD8544.h>

/*
// Software SPI (slower updates, more flexible pin options):
// pin 7 - Serial clock out (SCLK)
// pin 6 - Serial data out (DIN)
// pin 5 - Data/Command select (D/C)
// pin 4 - LCD chip select (CS)
// pin 3 - LCD reset (RST)
*/
Adafruit_PCD8544 display = Adafruit_PCD8544(7, 6, 5, 4, 3);
#define DHT11_PIN 2
#define BTN_DOWN 8
#define BTN_UP 9
#define BTN_ENTER 10


dht DHT;
RTC_DS1307 rtc;
int t =18;
int temp;
int hum;
int hysteresis = 1;

char daysOfTheWeek[7][12] = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};

/*
const static uint32_t RELAY_DELAY_AFTER_SWITCH_ON = 300000; // 5 minutes for testing | recommended 15-20 min...maybe 30 if low power furnace or very cold
const static uint32_t RELAY_DELAY_AFTER_SWITCH_OFF = 900000; // 15 minutes for testing | recommended 45 min. maybe less if loow power furnace or very cold
*/

/*
//Menu settings
#define MENU_LENGTH 4
#define MENU_ROW_HEIGHT 11
#define LCD_ROWS  4

int menuPos = 0;
int menuScreen = 0;
int markerPos = 0;
int menuStartAt = 0;
*/

void setup() {
	// put your setup code here, to run once:
	Serial.begin(9600);
	if (! rtc.begin()) {
		Serial.println("Couldn't find RTC");
		while (1);
	}

	if (! rtc.isrunning()) {
		Serial.println("RTC is NOT running!");
		// following line sets the RTC to the date & time this sketch was compiled
		rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
	}
	pinMode(BTN_UP, INPUT_PULLUP);
	pinMode(BTN_DOWN, INPUT_PULLUP);
	initialiseDisplay();
	showGreeting();
}

void loop() {
	// put your main code here, to run repeatedly:
	DateTime now = rtc.now(); //to be set every 1 minute for power save
	  Serial.print(now.year(), DEC);
    Serial.print('/');
    Serial.print(now.month(), DEC);
    Serial.print('/');
    Serial.print(now.day(), DEC);
    Serial.print(" (");
    Serial.print(daysOfTheWeek[now.dayOfTheWeek()]);
    Serial.print(") ");
    Serial.print(now.hour(), DEC);
    Serial.print(':');
    Serial.print(now.minute(), DEC);
    Serial.print(':');
    Serial.print(now.second(), DEC);
    Serial.println();
	readSensor();
	detectButtonState();
	refreshValues();
	//Serial.println(now.dayOfTheWeek());
	delay(1000);
}

//Senor/temperature math functions
void readSensor () {
	//code
	int chk = DHT.read11(DHT11_PIN);
	temp = DHT.temperature;
	hum = DHT.humidity;
}
//Sensor/temperature math functions

//Display functions
void refreshValues () {
	//code
	clearScreen();
	display.display();
	display.setCursor(0, 0);
	display.print("T: ");
	display.setCursor(36, 0);
	display.print(t);
	display.setCursor(0, 11);
	display.print("Temp: ");
	display.setCursor(36, 11);
	display.print(temp);
	display.setCursor(60, 11);
	display.print("C");
	display.setCursor(0, 22);
	display.print("Hum: ");
	display.setCursor(36, 22);
	display.print(hum);
	display.setCursor(60, 22);
	display.print("%");
	display.setCursor(0, 33);
	display.print("Heat: ");
	display.setCursor(36, 33);
	if (temp <= t) {
		display.print("ON");
	}
	else  {
		display.print("OFF");

	}
	display.display();
}

void clearScreen() {
	display.fillRect(0, 0, 84, 11, WHITE);
	display.fillRect(0, 11, 84, 11, WHITE);
	display.fillRect(0, 22, 84, 11, WHITE);
	display.fillRect(0, 33, 84, 11, WHITE);
}

void showGreeting() {
	display.setCursor(16, 12);
	display.print("ARDUtherm");
	display.setCursor(14, 28);
	display.print("Loading...");
	display.display();
	delay(5000);
	display.clearDisplay();
	display.display();
}

void initialiseDisplay() {
	display.begin();
	display.setContrast(50);
	display.setTextSize(1);
	display.clearDisplay();
	display.display();
}
//Display Functions

//Button functions
void detectButtonState () {
	if (isButtonDown(BTN_DOWN) == true) {
		if (t > 5) {
			t--;
		}
	}
	if (isButtonDown(BTN_UP) == true) {
		if (t < 30) {
			t++;
		}
	}
	if (isButtonDown(BTN_ENTER) == true) {
		selectItem();
	}
}

bool isButtonDown(int pin) {
	if (digitalRead(pin) == LOW) {
		delay(30);
		if (digitalRead(pin) == LOW) {
			return true;
		}
	return false;
	}
  return false;
}

void selectItem () {
	//code
}

//Button funtions
